from django.db import models
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _


class GroupRelationship(models.Model):
    allowed_groups = models.ManyToManyField(Group, verbose_name=_('Allowed groups'), related_name='allowed_groups')
    source_group = models.OneToOneField(Group, verbose_name=_('Source group'), related_name='source')
