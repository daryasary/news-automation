from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin as BaseGroupAdmin, \
    UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group, User

from relation.models import GroupRelationship


class AllowedGroupsInline(admin.TabularInline):
    model = GroupRelationship


class GroupAdmin(BaseGroupAdmin):
    inlines = (AllowedGroupsInline,)
    list_display = ['name', 'allowed_groups_property']

    def allowed_groups_property(self, obj):
        groups = obj.source.allowed_groups.values_list('name', flat=True)
        return ', '.join(groups)

    allowed_groups_property.short_description = 'Allowed groups'


class UserAdmin(BaseUserAdmin):
    list_display = BaseUserAdmin.list_display + ('get_user_groups', )

    def get_user_groups(self, obj):
        groups_names = obj.groups.values_list('name', flat=True)
        if groups_names:
            return ', '.join(groups_names)
        return '---'
    get_user_groups.short_description = 'Groups'

admin.site.unregister(Group)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Group, GroupAdmin)
