from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import Group, User
from django.shortcuts import redirect, render, get_object_or_404
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _

from news.models import Post
from transaction.models import PostLog


@staff_member_required
def change_post_ownership(request, pid, gid, dst=None):
    try:
        obj = Post.objects.get(pk=pid)
        group = Group.objects.get(pk=gid)
    except:
        messages.error(request, _('Wrong post or groups entered'))
        return redirect('/admin/news/post/')
    if dst is not None:
        dest = get_object_or_404(User, pk=dst)
        obj.status = Post.SUBMITTED
        obj.owner = dest
        obj.save()
        PostLog.set(request.user, obj, PostLog.TRANSFORMED)
        messages.success(
            request, 'Post sent successfully to the {}'.format(group.name)
        )
        return redirect('/news/post/')

    opts = Post._meta
    app_label = opts.app_label

    object_name = force_text(opts.verbose_name)
    title = _("Select target person from {} group.".format(group.name))
    users_list = group.user_set.all()

    context = dict(
        title=title,
        object_name=object_name,
        group_name=group.name,
        users_list=users_list,
        object=obj,
        opts=opts,
        app_label=app_label,
    )

    return render(request, 'admin/change_post_ownership.html', context)
