# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-01 06:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('news', '0004_post_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='owner',
            field=models.ForeignKey(default=1, editable=False, on_delete=django.db.models.deletion.CASCADE, related_name='posts', to='auth.Group', verbose_name='Owner'),
            preserve_default=False,
        ),
    ]
