from django.contrib import admin
from django import forms
from django.contrib.admin.exceptions import DisallowedModelAdminToField
from django.contrib.admin.options import TO_FIELD_VAR, IS_POPUP_VAR
from django.contrib.admin.utils import unquote, get_deleted_objects
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.db import router
from django.db.models import Q
from django.template.response import TemplateResponse
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.text import capfirst
from django.utils.translation import ugettext_lazy as _

from news.constants import django_admin_green_btn, transport_not_allowed_span
from news.models import Post, Category, Tag, PostAttachment

from ckeditor.widgets import CKEditorWidget

from transaction.models import PostLog


class PostAdminForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget())
    abstract = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Post
        fields = '__all__'


class TagAdmin(admin.ModelAdmin):
    list_display = ['name']


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'parent']


class AttachmentInlines(admin.TabularInline):
    model = PostAttachment
    extra = 1

    # Add a custom permission checking to django admin, to separate
    # posts which user can modify them with others
    def get_readonly_fields(self, request, obj=None):
        if obj and obj.owner != request.user:
            return tuple(['attachment', 'post', 'description'])
        return self.readonly_fields

    def has_delete_permission(self, request, obj=None):
        if obj and obj.owner != request.user:
            return False
        return True


class PostAdmin(admin.ModelAdmin):
    form = PostAdminForm
    list_display = ['title', 'author_fullname', 'owner_fullname', 'status', 'send_post_to_groups']
    object_history_template = 'admin/custom_object_history.html'
    inlines = (AttachmentInlines,)

    def save_model(self, request, obj, form, change):
        if not hasattr(obj, 'author'):
            obj.author = request.user
            obj.owner = request.user
            status = PostLog.CREATED
        else:
            status = PostLog.UPDATED
        super(PostAdmin, self).save_model(request, obj, form, change)
        PostLog.set(request.user, obj, status)

    def save_related(self, request, form, formsets, change):
        super(PostAdmin, self).save_related(request, form, formsets, change)
        PostLog.set(request.user, form.instance, PostLog.UPDATE_RELATED)

    def delete_model(self, request, obj):
        PostLog.set(request.user, obj, PostLog.DELETED)
        super(PostAdmin, self).delete_model(request, obj)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "owner":
            kwargs["queryset"] = Group.objects.filter(user=request.user)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super(PostAdmin, self).get_queryset(request)
        qs = qs.filter(Q(owner=request.user) | Q(author=request.user))
        return qs

    # Add a custom permission checking to django admin, to separate
    # posts which user can modify them with others
    def get_readonly_fields(self, request, obj=None):
        if obj and obj.owner != request.user:
            return tuple(
                ['title', 'abstract', 'body', 'status', 'author', 'owner',
                 'created_at', 'modified_at', 'tags', 'categories']
            )

        return self.readonly_fields

    def get_list_display(self, request):
        """
        Made a little hack on django admin default treat, request is appending
        to the PostAdmin instance to be used in send_post_to_groups
        """
        self.request = request
        return self.list_display

    def send_post_to_groups(self, obj):
        from django.core.urlresolvers import reverse
        if obj.owner != self.request.user:
            return format_html(transport_not_allowed_span)
        groups = obj.owner.groups.all()
        buttons = ''
        for group in groups:
            if hasattr(group, 'source'):
                allowed = group.source.allowed_groups.all()
                # Bypass space between two different groups of owner
                buttons += ' '
                buttons += ' '.join(
                    [django_admin_green_btn.format(
                        reverse(
                            'change_owner_ship_confirm', args=[obj.id, g.id]
                        ), g.name
                    ) for g in allowed]
                )
        return format_html('Send to: ' + buttons)
    send_post_to_groups.short_description = 'Available groups'

    def owner_fullname(self, obj):
        return obj.owner.get_full_name()
    owner_fullname.short_description = "owner"

    def author_fullname(self, obj):
        return obj.author.get_full_name()
    author_fullname.short_description = "author"

    def has_delete_permission(self, request, obj=None):
        if obj and obj.owner == request.user:
            return True
        return False

    def _delete_view(self, request, object_id, extra_context):
        opts = self.model._meta
        app_label = opts.app_label

        to_field = request.POST.get(TO_FIELD_VAR, request.GET.get(TO_FIELD_VAR))
        if to_field and not self.to_field_allowed(request, to_field):
            raise DisallowedModelAdminToField(
                "The field %s cannot be referenced." % to_field
            )

        obj = self.get_object(request, unquote(object_id), to_field)

        if not self.has_delete_permission(request, obj):
            raise PermissionDenied

        if obj is None:
            return self._get_obj_does_not_exist_redirect(request, opts, object_id)

        using = router.db_for_write(self.model)

        # Populate deleted_objects, a data structure of all related
        # objects that will also be deleted.
        (deleted_objects, model_count, perms_needed, protected) = get_deleted_objects(
            [obj], opts, request.user, self.admin_site, using)

        # The user has confirmed the deletion.
        if request.POST and not protected:
            if perms_needed:
                raise PermissionDenied
            obj_display = force_text(obj)
            attr = str(to_field) if to_field else opts.pk.attname
            obj_id = obj.serializable_value(attr)
            self.log_deletion(request, obj, obj_display)
            self.delete_model(request, obj)

            return self.response_delete(request, obj_display, obj_id)

        object_name = force_text(opts.verbose_name)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": object_name}
        else:
            title = _("Are you sure?")

        context = dict(
            self.admin_site.each_context(request),
            title=title,
            object_name=object_name,
            object=obj,
            # deleted_objects=deleted_objects,
            # model_count=dict(model_count).items(),
            perms_lacking=perms_needed,
            protected=protected,
            opts=opts,
            app_label=app_label,
            preserved_filters=self.get_preserved_filters(request),
            is_popup=(IS_POPUP_VAR in request.POST or
                      IS_POPUP_VAR in request.GET),
            to_field=to_field,
        )
        context.update(extra_context or {})

        return self.render_delete_form(request, context)

    def history_view(self, request, object_id, extra_context=None):
        model = self.model
        obj = self.get_object(request, unquote(object_id))
        if obj is None:
            return self._get_obj_does_not_exist_redirect(
                request, model._meta, object_id
            )

        if not self.has_change_permission(request, obj):
            raise PermissionDenied

        # Then get the history for this object.
        opts = model._meta
        app_label = opts.app_label
        action_list = PostLog.objects.filter(post=obj)

        context = dict(
            self.admin_site.each_context(request),
            title=_('Change history: %s') % force_text(obj),
            action_list=action_list,
            module_name=capfirst(force_text(opts.verbose_name_plural)),
            object=obj,
            opts=opts,
            preserved_filters=self.get_preserved_filters(request),
        )

        request.current_app = self.admin_site.name
        return TemplateResponse(request, self.object_history_template, context)


admin.site.register(Post, PostAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Category, CategoryAdmin)
