from django.conf.urls import url

from news.views import change_post_ownership

urlpatterns = [
    url(r'^change_ownership/(?P<pid>.*)/(?P<gid>.*)/(?P<dst>.*)/$', change_post_ownership, name='change_owner_ship'),
    url(r'^change_ownership/(?P<pid>.*)/(?P<gid>.*)/$', change_post_ownership, name='change_owner_ship_confirm'),
]
