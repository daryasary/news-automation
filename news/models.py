import json

from django.contrib.auth.models import User, Group
from django.db import models
from django.utils.translation import ugettext_lazy as _


class PostManager(models.Manager):
    def get_queryset(self):
        return super(PostManager, self).get_queryset().exclude(status=10)


class Tag(models.Model):
    name = models.CharField(max_length=32, verbose_name=_('Name'))

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=32, verbose_name=_('Name'))
    parent = models.ForeignKey('self', blank=True, null=True)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __str__(self):
        if self.parent is None:
            return self.name
        return '{} > {}'.format(self.parent, self.name)


class Post(models.Model):
    CREATED = 1
    SUBMITTED = 2
    APPROVED = 5
    DELETED = 10
    STATUS_CHOICES = [
        (CREATED, _("Created")), (SUBMITTED, _("Submitted")),
        (APPROVED, _("Approved")), (DELETED, _("Deleted"))
    ]

    title = models.CharField(max_length=128, verbose_name=_('Title'))
    # slug = models.SlugField()  # Delete if no error happened
    abstract = models.TextField(verbose_name=_('Abstract'), blank=True)
    body = models.TextField(verbose_name=_('Body'))

    status = models.IntegerField(
        verbose_name=_('Status'),
        choices=STATUS_CHOICES,
        editable=False,
        default=CREATED
    )

    author = models.ForeignKey(
        User, verbose_name=_('Author'), related_name='posts', editable=False
    )
    owner = models.ForeignKey(
        User, verbose_name=_('Current Owner'), related_name='tasks', editable=False
    )

    tags = models.ManyToManyField(
        Tag, verbose_name=_('Tags'), related_name='posts'
    )
    categories = models.ManyToManyField(
        Category, verbose_name=_('Categories'), related_name='posts'
    )

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    objects = PostManager()
    complete = models.Manager()

    class Meta:
        verbose_name = _("Post")
        verbose_name_plural = _("Posts")
        permissions = (
            ("can_publish_post", "Can publish post"),
        )

    def __str__(self):
        return self.title

    @property
    def data(self):
        # TODO: ADD Tags and Categories
        return json.dumps(
            {
                'id': self.id,
                'title': self.title,
                'owner': self.owner.username,
                'abstract': self.abstract,
                'body': self.body
            }
        )

    @property
    def attach_data(self):
        attachments = self.attachments.all()
        attach_list = list()
        for attach in attachments:
            attach_list.append({
                'url': attach.attachment.url,
                'desc': attach.description,
            })
        return attach_list

    def delete(self, using=None, keep_parents=False):
        # super(Post, self).delete(using, keep_parents)
        self.status = self.DELETED
        self.save()


class PostAttachment(models.Model):
    attachment = models.FileField(upload_to='attachments/')
    post = models.ForeignKey(Post, related_name='attachments')
    description = models.TextField(blank=True)
