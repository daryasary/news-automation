from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User, Group
from django.contrib.postgres.fields import JSONField
from django.db import models

from news.models import Post


class PostLog(models.Model):
    CREATED = 1
    UPDATED = 2
    TRANSFORMED = 3
    DELETED = 4
    UPDATE_RELATED = 5
    STATUS_CHOICES = (
        (CREATED, _("Created")),
        (UPDATED, _("Updated")),
        (TRANSFORMED, _("Transformed")),
        (DELETED, _("Deleted")),
        (UPDATE_RELATED, _("Update attachments"))
    )
    user = models.ForeignKey(User, related_name='posts_logs')
    post = models.ForeignKey(Post, related_name='posts_logs')
    owner = models.ForeignKey(User, related_name='task_logs')
    data = JSONField(blank=True, null=True)
    attachments = JSONField(blank=True, null=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=UPDATED)

    time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.user.username)

    @classmethod
    def set(cls, user, post, status):
        cls.objects.create(
                user=user, post=post, owner=post.owner, data=post.data,
                status=status, attachments=post.attach_data
            )
