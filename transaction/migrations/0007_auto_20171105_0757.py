# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-05 07:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('transaction', '0006_auto_20171105_0656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postlog',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='logs', to='auth.Group'),
        ),
    ]
