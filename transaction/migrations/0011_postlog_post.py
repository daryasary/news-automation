# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-18 08:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0009_auto_20171115_1018'),
        ('transaction', '0010_auto_20171115_1018'),
    ]

    operations = [
        migrations.AddField(
            model_name='postlog',
            name='post',
            field=models.ForeignKey(default=27, on_delete=django.db.models.deletion.CASCADE, related_name='posts_logs', to='news.Post'),
            preserve_default=False,
        ),
    ]
