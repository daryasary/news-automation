from django.contrib import admin

from transaction.models import PostLog


class PostLogAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'owner', 'status', 'time'
    )

    def has_add_permission(self, request):
        return False


# admin.site.register(PostLog, PostLogAdmin)
